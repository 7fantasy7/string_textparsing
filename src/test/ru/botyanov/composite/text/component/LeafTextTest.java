package test.ru.botyanov.composite.text.component;

import org.junit.Assert;
import org.junit.Test;
import ru.botyanov.composite.text.component.ComponentType;
import ru.botyanov.composite.text.component.LeafText;

public class LeafTextTest {

    @Test
    public void testAdd() {
        boolean expected = false;
        LeafText leafText = new LeafText(ComponentType.WORD,"hello");
        boolean actual = leafText.add(new LeafText(ComponentType.WORD,"bye"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testRemove() {
        boolean expected = false;
        LeafText leafText = new LeafText(ComponentType.WORD,"hello");
        boolean actual = leafText.remove(new LeafText(ComponentType.WORD,"hello"));
        Assert.assertEquals(expected, actual);
    }
}