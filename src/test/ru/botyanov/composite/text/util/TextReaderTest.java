package test.ru.botyanov.composite.text.util;

import org.junit.Test;
import ru.botyanov.composite.text.util.TextReader;

public class TextReaderTest {

    @Test( expected = RuntimeException.class )
    public void testReadFromFile()  throws RuntimeException {
        TextReader.readFromFile("/input/input.txt");
    }
}