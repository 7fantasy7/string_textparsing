package ru.botyanov.composite.text.parser;

import org.apache.log4j.Logger;
import ru.botyanov.composite.text.component.ComponentType;
import ru.botyanov.composite.text.component.CompositeText;
import ru.botyanov.composite.text.component.IComponent;
import ru.botyanov.composite.text.component.LeafText;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextParser {
    public static final Logger LOG = Logger.getLogger(TextParser.class);

    public static CompositeText parse(StringBuilder text) {
        LOG.debug("Started parsing");
        return parse(text, ComponentType.TEXT);
    }

    private static CompositeText parse(StringBuilder text, ComponentType parserMode) {
        LinkedList<IComponent> components = new LinkedList<>();

        Pattern pattern = Pattern.compile(parserMode.getPattern());
        Matcher matcher = pattern.matcher(text);

        switch (parserMode) {
            case TEXT:
                while (matcher.find()) {
                    String group = matcher.group();
                    if (group.startsWith("/*")) {
                        components.add(new LeafText(ComponentType.LISTING, group));
                    } else {
                        components.add(parse(new StringBuilder(group), ComponentType.PARAGRAPH));
                    }
                }
                break;
            case PARAGRAPH:
                while (matcher.find()) {
                    String group = matcher.group();
                    components.add(parse(new StringBuilder(group), ComponentType.SENTENCE));
                }
                break;
            case SENTENCE:
                while (matcher.find()) {
                    String group = matcher.group();
                    if (group.matches(PatternConstants.WORD_PATTERN)) {
                        components.add(new LeafText(ComponentType.WORD, group));
                    } else {
                        components.add(parse(new StringBuilder(group), ComponentType.LEXEM));
                    }
                }
                break;
            case LEXEM:
                while (matcher.find()) {
                    String word = matcher.group(1);
                    String sign = matcher.group(2);
                    if(!word.isEmpty()) {
                        components.add(new LeafText(ComponentType.WORD, word));
                    }
                    components.add(new LeafText(ComponentType.PUNCT, sign));
                }
                break;
            default:
                LOG.error("Illegal parseMode:" + parserMode);
        }
        return new CompositeText(components, parserMode);
    }
}