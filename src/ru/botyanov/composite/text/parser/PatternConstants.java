package ru.botyanov.composite.text.parser;

public class PatternConstants {
    public static final String TEXT_PATTERN = "\\t[^\\Q/\\*\\E\t]+|((?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*))";//to paragraphs
    public static final String PARAGRAPH_PATTERN = "[\\p{L}\\s\\p{P}<>]+[.?!:]";//to sentences
    public static final String LEXEM_PATTERN = "([\\p{L}\\s]*)([\\p{P}<>]{1})";//to word + punct
    public static final String SENTENCE_PATTERN = "\\p{L}+((\\([^)]*\\))|(\\<[^>]*\\>))*\\p{P}*";//to words
    public static final String WORD_PATTERN = "\\p{L}+";
}