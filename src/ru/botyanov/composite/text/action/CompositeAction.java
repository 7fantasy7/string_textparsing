package ru.botyanov.composite.text.action;

import org.apache.log4j.Logger;
import ru.botyanov.composite.text.component.ComponentType;
import ru.botyanov.composite.text.component.CompositeText;

import java.util.ArrayList;

public class CompositeAction {
    public static final Logger LOG = Logger.getLogger(CompositeAction.class);

    public static void alphabetSorting(CompositeText text) {
        ArrayList<String> alphabetSortedList = new ArrayList<>();
        getAllWords(text, alphabetSortedList);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < alphabetSortedList.size(); i++) {
            builder.append(alphabetSortedList.get(i)).append(" ");
            if (i < alphabetSortedList.size() - 1 &&
                    !alphabetSortedList.get(i + 1).toLowerCase().startsWith((alphabetSortedList.get(i).substring(0, 1)).toLowerCase())) {
                builder.append('\n');
            }
        }
        LOG.debug(builder);
    }

    public static void serching(CompositeText text, char ch) {
        ArrayList<String> allWords = new ArrayList<>();
        getAllWords(text, allWords);
        allWords.stream().filter((s) -> s.toLowerCase().contains(String.valueOf(ch))).
                sorted((o1, o2) -> getCount(ch, o2) - getCount(ch, o1)).forEach(LOG::debug);
    }

    private static int getCount(char ch, String o1) {
        int counter = 0;
        char[] string = o1.toLowerCase().toCharArray();
        for (char c : string) {
            if (c == ch) {
                counter++;
            }
        }
        return counter;
    }

    private static void getAllWords(CompositeText text, ArrayList<String> allWords) {
        text.getElements().stream().filter(i -> !i.getType().equals(ComponentType.LISTING)).
                flatMap(i -> i.getElements().stream()).
                flatMap(i -> i.getElements().stream()).
                flatMap(i -> i.getElements().stream()).
                filter(i -> i.getType().equals(ComponentType.WORD)).
                map(Object::toString).
                map(String::toLowerCase).
                sorted().
                forEach(allWords::add);
    }

}