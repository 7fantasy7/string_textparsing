package ru.botyanov.composite.text.util;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TextReader {
    private static Logger LOG = Logger.getLogger(TextReader.class);

    public static StringBuilder readFromFile(String fileName) {
        StringBuilder text;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            text = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                text.append(line);
                text.append(System.getProperty("line.separator"));
            }

        } catch (FileNotFoundException e) {
            LOG.fatal(e + ":file " + fileName + " not found");
            throw new RuntimeException(e);
        } catch (IOException e) {
            LOG.fatal(e + "file reading failed");
            throw new RuntimeException(e);
        }
        return text;
    }
}