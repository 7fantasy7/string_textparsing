package ru.botyanov.composite.text.component;

import java.util.LinkedList;

public interface IComponent {
    boolean add(IComponent elem);

    boolean remove(IComponent elem);

    LinkedList<IComponent> getElements();

    ComponentType getType();
}
