package ru.botyanov.composite.text.component;

import java.util.Arrays;
import java.util.LinkedList;

public class LeafText implements IComponent {
    private ComponentType leafType;
    private String text;

    public LeafText(ComponentType leafType, String text) {
        this.leafType = leafType;
        this.text = text;
    }

    @Override
    public boolean add(IComponent elem) {
        return false;
    }

    @Override
    public boolean remove(IComponent elem) {
        return false;
    }

    @Override
    public LinkedList<IComponent> getElements() {
        return new LinkedList<>(Arrays.asList(this));
    }

    @Override
    public ComponentType getType() {
        return leafType;
    }

    @Override
    public String toString() {
        String textToString;
        if (leafType == ComponentType.LISTING) {
            textToString = text + System.getProperty("line.separator");
        } else {
            textToString = text;
        }
        return textToString;
    }

}