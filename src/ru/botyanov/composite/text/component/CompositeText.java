package ru.botyanov.composite.text.component;

import java.util.Iterator;
import java.util.LinkedList;

public class CompositeText implements IComponent {
    private ComponentType compotiseType;
    private LinkedList<IComponent> elements;

    public CompositeText(LinkedList<IComponent> elements, ComponentType compotiseType) {
        this.elements = elements;
        this.compotiseType = compotiseType;
    }

    @Override
    public boolean add(IComponent squad) {
        return elements.add(squad);
    }

    @Override
    public boolean remove(IComponent squad) {
        return elements.remove(squad);
    }

    @Override
    public ComponentType getType() {
        return compotiseType;
    }

    @Override
    public LinkedList<IComponent> getElements() {
        return elements;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (compotiseType == ComponentType.PARAGRAPH) {
            stringBuilder.append("\t");
        }
        Iterator<IComponent> iterator = getElements().iterator();
        while (iterator.hasNext()) {
            stringBuilder.append(iterator.next());
            if (iterator.hasNext() && compotiseType != ComponentType.LEXEM) {
                stringBuilder.append(" ");
            }
        }
        if (compotiseType == ComponentType.PARAGRAPH) {
            stringBuilder.append(System.getProperty("line.separator"));
        }
        return stringBuilder.toString();
    }
}