package ru.botyanov.composite.text.component;

import ru.botyanov.composite.text.parser.PatternConstants;

public enum ComponentType {
    WORD(),
    LISTING(),
    PUNCT(),
    LEXEM(PatternConstants.LEXEM_PATTERN),
    SENTENCE(PatternConstants.SENTENCE_PATTERN),
    PARAGRAPH(PatternConstants.PARAGRAPH_PATTERN),
    TEXT(PatternConstants.TEXT_PATTERN);

    private String pattern;

    ComponentType() {
    }

    ComponentType(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }

}
