package ru.botyanov.composite.text.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import ru.botyanov.composite.text.action.CompositeAction;
import ru.botyanov.composite.text.component.*;
import ru.botyanov.composite.text.parser.TextParser;
import ru.botyanov.composite.text.util.TextReader;

public class Runner {
    public static final Logger LOG = Logger.getLogger(Runner.class);
    private static final String LOG_PATH = "config/log4j.xml";
    private static final String FILE_PATH = "files/input.txt";

    static {
        new DOMConfigurator().doConfigure(LOG_PATH, LogManager.getLoggerRepository());
    }

    public static void main(String[] args) {
        StringBuilder stringBuilder = TextReader.readFromFile(FILE_PATH);
        CompositeText compositeText = TextParser.parse(stringBuilder);
        LOG.debug("Parsed text\n" + compositeText);
        CompositeAction.alphabetSorting(compositeText);
        CompositeAction.serching(compositeText, 'в');
    }
}